from flask_wtf import FlaskForm
from wtforms import StringField,PasswordField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length, EqualTo,URL
from wtfpeewee.orm import model_form
from models import Utilisateur, Abonnement

SimpleUtilisateurForm = model_form(Utilisateur)

class UtilisateurForm(FlaskForm):
    name = StringField('Nom', validators=[
                        DataRequired(), Length(min=3, max=20)])
    password = PasswordField('Mot de passe', validators=[
                        DataRequired(), Length(min=3, max=20),
                        EqualTo('confirm', message='Les mots de passes sont différents')])
    confirm = PasswordField('Repeat Password')


SimpleAbonnementForm = model_form(Abonnement)

class AbonnementForm(FlaskForm):
    title=StringField('Nom', validators=[
                        DataRequired(), Length(min=3, max=20)])
    rss=StringField('Flux', validators=[
                        URL(message='Le Flux doit être une URL')])
