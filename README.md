# Flux RSS/Atom Guillaume Zieba - Domont Henry - Gillot Yann

Afin de valider nos compétences en Python, un projet nous a été donné : concevoir un lecteur de Flux RSS/Atom

## Lancement

Pour lancer l'outil, rien de plus simple, exécutez le fichier **debug.bat** qui sert à lancer le projet.


## Explications

Une fois sur le site, on vous proposera de vous connecter ou de vous enregistrer (Il est vérifié à l'enregistrement que l'utilisateur n'existe pas déjà). Nous vous invitons à créer un compte ou prendre l'un de ceux existant donné dans la rubrique **Comptes**.
Une fois connecté, vous avez accès à différentes pages :

 - L'accueil qui ressence tous vos flux RSS
 - La page de vos flux qui ressence vos Flux souscrient et vous permet d'en voir un en particulier ou de supprimer un flux.
 - La page de souscription à un Flux où il vous suffit de mettre un nom et l'URL de votre Flux
 - La gestion du compte qui vous permet de visualiser et modifier votre compte

```
(Une page bonus accessible mais non implémenté dans l'interface permet de voir tous les utilisateurs (/utilisateur/showall))
```


## Affichage des articles des flux
Nous avons choisi un affichage simple en ligne avec une image, le titre, la description, le logo de la source, la date de publication et un bouton redirigeant sur l'article complet. Chaque flux est séparé par le titre du flux et son sous-titre.

## Responsive
Nous tenons à préciser que tout le site est responsive et donc accessible depuis PC,Smarthpone,Tablette(,TV?)

## Comptes
Pour vous connecter, nous avons créer 2 utilisateurs : 

 1. id = Yann, mdp = test (abonné à 2 flux) 
 2. id = test, mdp = test (abonné à aucun aucun flux)

Aucun mot de passe n'est stocké en clair, ils sont hashé.

Libre à vous de recréer un ou des comptes
