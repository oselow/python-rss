from flask import Flask, flash, redirect, render_template, request, session, abort, url_for
from models import Utilisateur, Abonnement, create_tables, drop_tables
from forms import UtilisateurForm, AbonnementForm, SimpleUtilisateurForm, SimpleAbonnementForm
from dateutil import parser
from datetime import datetime
import os
import click
import feedparser
import array
from hashlib import sha512

app = Flask(__name__)
app.secret_key = os.urandom(12)

@app.template_filter('dt')
def _jinja2_filter_datetime(date):
    return parser.parse(date).strftime("%d/%m/%Y %H:%M:%S")

@app.cli.command()
def initdb():
    """Create database"""
    create_tables()
    click.echo('Initialized the database')

@app.cli.command()
def dropdb():
    """Drop database tables"""
    drop_tables()
    click.echo('Dropped tables from database')


@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        try:
            utilisateur = Utilisateur.get(id=session.get('user'))
        except Utilisateur.DoesNotExist:
            return logout()

        fluxs=[]
        for flux in utilisateur.abonnements:
            fluxs.append(feedparser.parse(flux.rss))
            
        return render_template('abonnement/display.html', fluxs=fluxs)


@app.route('/login', methods=['POST'])
def login():
    # if request.form['password'] == 'password' and request.form['username'] == 'admin':
    #     session['logged_in'] = True
    # else:
    #     flash('wrong password!')
    # return home()
    try:
        password = sha512((request.form['password']).encode()).hexdigest()
        utilisateur = (Utilisateur
                        .select()
                        .where(Utilisateur.name == request.form['username'] and Utilisateur.password == password)
                        .get())
        session['user'] = utilisateur.id
        session['logged_in'] = True
    except Utilisateur.DoesNotExist:
        flash('Mauvais mot de passe!')
        
    return home()


@app.route("/logout")
def logout():
    session['logged_in'] = False
    return home()


@app.route('/register', methods=['GET', 'POST', ])
def utilisateur_create():
    utilisateur = Utilisateur()
    form = UtilisateurForm()
    if form.validate_on_submit():
        try:
            (Utilisateur
                .select()
                .where(Utilisateur.name == request.form['name'])
                .get())
            exist = True
        except Utilisateur.DoesNotExist:
            exist = False
        
        if exist:
            flash ('L\'utilisateur existe déjà')
        else:
            form.populate_obj(utilisateur)
            utilisateur.password = sha512((request.form['password']).encode()).hexdigest()
            utilisateur.save()
            return redirect(url_for('home'))
    return render_template('form.html', form=form)


@app.route('/utilisateur/showall')
def utilisateur_showall():
    liste = Utilisateur.select()
    return render_template('utilisateur/liste.html', liste=liste)


@app.route('/utilisateur/show/<int:utilisateur_id>')
def utilisateur_show(utilisateur_id):
    try:
        utilisateur = Utilisateur.get(id=utilisateur_id)
    except Utilisateur.DoesNotExist:
        abort(404)

    return render_template('utilisateur/show.html', user=utilisateur)


@app.route('/utilisateur/edit/<int:utilisateur_id>', methods=['GET', 'POST'])
def utilisateur_edit(utilisateur_id):
    try:
        utilisateur = Utilisateur.get(id=utilisateur_id)
    except Utilisateur.DoesNotExist:
        abort(404)

    form = UtilisateurForm(request.form, obj=utilisateur)
    if form.validate_on_submit():
        try:
            (Utilisateur
                .select()
                .where(Utilisateur.name == request.form['name'])
                .get())
            exist = True
        except Utilisateur.DoesNotExist:
            exist = False
        
        if exist and request.form['name'] != utilisateur.name:
            flash ('L\'utilisateur existe déjà')
        else:
            form.populate_obj(utilisateur)
            utilisateur.password = sha512((request.form['password']).encode()).hexdigest()
            utilisateur.save()
            flash('Modifications sauvegardées')
            return redirect(url_for('utilisateur_show', utilisateur_id=utilisateur.id))

    return render_template('form.html', form=form)


@app.route('/abonnement/show/<int:abonnement_id>')
def abonnement_show(abonnement_id):
    try:
        abonnement = Abonnement.get(id=abonnement_id)
    except Abonnement.DoesNotExist:
        abort(404)

    fluxs = []
    fluxs.append((feedparser.parse(abonnement.rss)))
    return render_template('abonnement/display.html', fluxs=fluxs)


@app.route('/abonnement/delete/<int:abonnement_id>')
def abonnement_delete(abonnement_id):
    try:
        abonnement = Abonnement.get(id=abonnement_id)
    except Abonnement.DoesNotExist:
        abort(404)

    if abonnement.user.id == session.get('user'):
        abonnement.delete_instance(recursive=True)
        flash(abonnement.title+" supprimé")
        return redirect(url_for('utilisateur_abonnements'))
    else:
        abort(404)

@app.route('/mesAbonnements')
def utilisateur_abonnements():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        try:
            utilisateur = Utilisateur.get(id=session.get('user'))
        except Utilisateur.DoesNotExist:
            return render_template('login.html')

        liste = utilisateur.abonnements
        return render_template('abonnement/liste.html', liste=liste)


@app.route('/abonne', methods=['GET', 'POST'])
def abonne():
    try:
        Utilisateur.get(id=session.get('user'))
    except Utilisateur.DoesNotExist:
        return render_template('login.html')

    abonnement = Abonnement()
    form = AbonnementForm()
    if form.validate_on_submit():
        form.populate_obj(abonnement)
        abonnement.user=session.get('user')
        abonnement.save()
        flash('Abonnement '+ abonnement.title +' créé')
        return redirect(url_for('utilisateur_abonnements'))
    return render_template('form.html', form=form)