from peewee import SqliteDatabase, CharField, DateTimeField, Model, ForeignKeyField
import datetime

database = SqliteDatabase("flux.sqlite3")

class BaseModel(Model):

    class Meta:
        database = database

class Utilisateur(BaseModel):
    name = CharField()
    password = CharField()
    created_at = DateTimeField(default=datetime.datetime.now)

class Abonnement(BaseModel):
    user = ForeignKeyField(Utilisateur, backref="abonnements")
    title = CharField()
    rss = CharField()
    created_at = DateTimeField(default=datetime.datetime.now)


def create_tables():
    with database:
        database.create_tables([Utilisateur,Abonnement])


def drop_tables():
    with database:
        database.drop_tables([Utilisateur,Abonnement])